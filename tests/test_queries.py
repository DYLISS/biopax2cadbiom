# -*- coding: utf-8 -*-
# MIT License
#
# Copyright (c) 2017 IRISA, Jean Coquet, Pierre Vignet, Mateo Boudet
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#
# Contributor(s): Jean Coquet, Pierre Vignet, Mateo Boudet

"""
This module is used to test BioPAX SPARQL queries.

.. note:: To add a test, please add it to test_pool and result_pool dictionnaries.
    key: name of the test,
    value: tuple of uris or expected result.

"""

from __future__ import unicode_literals
from __future__ import print_function

# Standard imports
from functools import partial
from collections import Counter

# Custom imports
from biopax2cadbiom import sparql_biopaxQueries as query

# Tests and params
# tuple: 0: list of uris
test_pool = {
        'virtualCase7': ('http://biopax.org/lvl3', 'http://virtualcases.org/7'),
        'virtualCase8': ('http://biopax.org/lvl3', 'http://virtualcases.org/8'),
    }
# For each entity: the Counter of modification features
result_pool = {
        'virtualCase7': {},
        'virtualCase8': {
            'http://virtualcases.org/8#A': Counter({'phosphorylated': 1}),
            'http://virtualcases.org/8#B': Counter({'phosphorylated': 2}),
            'http://virtualcases.org/8#C': Counter({
                'phosphorylated': 2,
                'acetylated': 1,
                'ubiquitinated': 1,
            }),
        }
    }


def test_xrefs_query():
    """Test the retrieving of xrefs of all entities in the database

    Entities that have xref attributes in addition to their entityReference
    xrefs are supported.

    UnificationXref, RelationshipXref are supported.

    Classes inherit xref from their members via their entityReference attributes
    that have memberEntityReference attributes.
    """

    dictEntities_db_refs = query.get_biopax_xrefs(
        ('http://biopax.org/lvl3', 'http://virtualcases.org/12'),
        None
    )

    expected = {
        'http://virtualcases.org/12#Class_A': {
            'database_1': ['Y'],
            'database_2': ['Z']
        },
        'http://virtualcases.org/12#Protein_A': {
            'database_0': ['X'],
            'database_1': ['Y'],
            'database_2': ['Z']
        }
    }

    assert dictEntities_db_refs == expected


def t_modification_features(model_name, uris, expected_result):
    """Test if queried modifications features are similar to expected ones.
    """

    def count_modifications_features(query_result):
        """Count modification features for all entities in the query

        .. note:: Not used.
        """
        nb_modifs = 0
        for terms in query_result.itervalues():
            nb_modifs += sum(terms.itervalues())
        return nb_modifs

    dictModificationFeatures = query.get_biopax_modificationfeatures(uris, None)

    assert dictModificationFeatures == expected_result


for specie, uris in test_pool.iteritems():
    """Create test functions based on test_pool variable."""
    func = partial(
        t_modification_features,
        model_name=specie,
        uris=uris,
        expected_result=result_pool[specie],
    )
    globals()['test_' + specie] = func

