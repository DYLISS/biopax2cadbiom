# -*- coding: utf-8 -*-
# MIT License
#
# Copyright (c) 2017 IRISA, Jean Coquet, Pierre Vignet, Mateo Boudet
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#
# Contributor(s): Jean Coquet, Pierre Vignet, Mateo Boudet
"""
This module contains some helpful functions not related to the interpretation
or the conversion of the BioPAX data.
"""
# Standard imports
from __future__ import unicode_literals
from __future__ import print_function
from collections import Counter
from textwrap import wrap

# Custom imports
import biopax2cadbiom.commons as cm
import sparql_biopaxQueries # Syntax: Fix cyclic import

LOGGER = cm.logger()


def parse_uri(uri):
    """Return the suffix of the given URI or of the URIs in tuples or list

    :param uri: Uri or iterable of uris
    :type uri: <tuple> or <list> or <set>
    :return: String of uris without their prefix.
    :rtype: <str>
    """
    if isinstance(uri, basestring):
        try:
            return str(uri.rsplit("#", 1)[1])
        except IndexError:
            return str(uri.rsplit("/", 1)[1])

    elif isinstance(uri, tuple) or isinstance(uri, list) or isinstance(uri, set):
        return str(uri.__class__([parse_uri(i) for i in uri]))
    else:
        return uri


def get_info_from_triplestore(params):
    """Display informations about the graphs stored on the triplestore.

    We try to remove duplicates and print as much graphs and subgraphs as possible.
    If displayed, the element "Provenance URI" is necessary to query subgraphs
    in a triplestore.
    """
    # Setup the triplestore
    cm.SPARQL_PATH = params['triplestore']

    # Get tuples: (graph_uri, provenance_uri, name, display_name, comment)
    for graph_uri, provenance_uri, name, display_name, comment in sparql_biopaxQueries.get_info_from_triplestore():
        if comment and "REPLACED" in comment:
            # Don't print duplicates with URI redirection
            continue

        # Name formatting
        # Note: In practice, name is more precise than displayName.
        if display_name and name:
            if display_name != name:
                # display_name has additional info
                name = "%s (%s)" % (name, display_name)
        else:
            name = name if name else display_name

        # Comment formatting
        if comment:
            comment = "\n\t".join(wrap(comment, width=85, break_long_words=False))

        if provenance_uri and '#' not in provenance_uri:
            # Provenance uri = PathwayCommons (need to query subgraphs)
            text = \
            "Graph URI: {}\n\t" \
            "Provenance URI: {}\n\t" \
            "Name: {}\n\t" \
            "Comment: {}\n\t" \
            "---".format(graph_uri, provenance_uri, name, comment)
        else:
            text = \
            "Graph URI: {}\n\t" \
            "Name: {}\n\t" \
            "Comment: {}\n\t" \
            "---".format(graph_uri, name, comment)
        print(text)


def get_metrics_from_data(dictControl, dictReaction, dictPhysicalEntity, dictPathwayName):
    """Display multiple data about the queried data and the enrichment results"""

    ## Data on Controls
    interaction_types = Counter()
    nb_controls_with_multiple_controllers = 0
    nb_controlled_controls = 0
    nb_controllers_not_entities = 0

    for control in dictControl.values():
        interaction_types[control.interactionType] += 1

        if control.controlled in dictControl:
            nb_controlled_controls += 1

        if len(control.controllers) > 1:
            nb_controls_with_multiple_controllers += 1

        # Compute controllers that are not PhysicalEntities
        # This is mandatory in the norm; but who knows ?
        nb_controllers_not_entities += sum(
            [True for controller in control.controllers
             if controller not in dictPhysicalEntity]
        )

    LOGGER.info("Nb controls: %s", len(dictControl))
    LOGGER.info(
        "Instances count of control classes:\n%s",
        ", ".join(
            "%s: %d" % class_type_count for class_type_count in interaction_types.iteritems()
        )
    )
    LOGGER.info("Nb controls with multiple controllers: %s", nb_controls_with_multiple_controllers)
    LOGGER.info("Nb controls that control other controls: %s", nb_controlled_controls)
    LOGGER.info(
        "Nb controllers in controls that are not entities "
        "(this is a bug, should be 0): %s",
        nb_controllers_not_entities
    )

    # Check how many times an element (not a reaction) is controlled by a Control
    check_dups = Counter([
        control.controlled for control in dictControl.itervalues()
        if control.controlled not in dictReaction
    ])
    # Display if controlled more than 1 time
    for controlled_control, occurrences in check_dups.iteritems():
        if occurrences > 1:
            LOGGER.info(
                "Control <%s> is controlled %s times",
                controlled_control,
                occurrences
            )
    assert sum(check_dups.itervalues()) == nb_controlled_controls

    ## Data on entities and reactions
    # Detect how many Proteins and SmallMolecules are used in reactions
    prots_in_reactions = set()
    smallmol_in_reactions = set()
    complexes_in_reactions = set()
    classes_in_controls = set()
    for reaction in dictReaction.itervalues():
        reactants = reaction.leftComponents | reaction.rightComponents

        for reactant_uri in reactants:
            reactant = dictPhysicalEntity[reactant_uri]
            entity_type = reactant.entityType
            if entity_type == "Protein":
                prots_in_reactions.add(reactant_uri)
            if entity_type == "SmallMolecule":
                smallmol_in_reactions.add(reactant_uri)
            if entity_type == "Complex":
                complexes_in_reactions.add(reactant_uri)

        for control in reaction.controllers:
            for controller in control.controllers:
                try:
                    entity = dictPhysicalEntity[controller]
                    if entity.members:
                        classes_in_controls.add(entity)
                except KeyError:
                    # Compute controllers that are not PhysicalEntities
                    # This is mandatory in the norm; but who knows ?
                    pass

    LOGGER.info("Nb unique pathways: %s", len(set(dictPathwayName.values())))
    LOGGER.info("Nb reactions: %s", len(dictReaction))
    LOGGER.info("Nb physical entities: %s", len(dictPhysicalEntity))
    LOGGER.info("Nb proteins in reactions: %s", len(prots_in_reactions))
    LOGGER.info("Nb SmallMolecules in reactions: %s", len(smallmol_in_reactions))
    LOGGER.info("Nb complexes in reactions: %s", len(complexes_in_reactions))
    LOGGER.info("Nb classes in controls: %s", len(classes_in_controls))

    ## Data on classes
    # Count classes
    nb_classes = 0
    nb_classes_used = 0
    nb_classes_modifs = 0
    for entity in dictPhysicalEntity.itervalues():

        if entity.members:
            nb_classes += 1

            if entity.modificationFeatures:
                nb_classes_modifs += 1

        if entity.membersUsed:
            nb_classes_used += 1

    LOGGER.info("Nb classes: %s", nb_classes)
    LOGGER.info("Nb classes with members used in reactions: %s", nb_classes_used)
    LOGGER.info("Nb classes w/ modificationFeatures: %s", nb_classes_modifs)

    # Detect if complexes/classes can have components
    nb_complexes_classes_with_components = 0
    nb_complexes_classes = 0
    for entity in dictPhysicalEntity.itervalues():

        if not entity.is_complex:
            continue

        if entity.members:
#            print(entity.uri)
#            print("flat", entity.flat_components)
#            print("members", entity.members)
#            print()
            nb_complexes_classes += 1
            if entity.components_uris:
#                print(entity)
                nb_complexes_classes_with_components += 1

    LOGGER.info("Nb complexes/classes: %s", nb_complexes_classes)
    LOGGER.info("Nb complexes/classes with components: %s", nb_complexes_classes_with_components)

    # Count nested classes
    nb_nested_classes = 0
    for entity in dictPhysicalEntity.itervalues():
        # Parent
        for member_uri in entity.members:
            member_entity = dictPhysicalEntity[member_uri]

            if member_entity.members:

                # print("parent: %s, child: %s" % (entity, member_entity))
                nb_nested_classes += 1
    LOGGER.info("Nb nested classes: %s", nb_nested_classes)
