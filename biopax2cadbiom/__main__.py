# -*- coding: utf-8 -*-
# MIT License
#
# Copyright (c) 2017 IRISA, Jean Coquet, Pierre Vignet, Mateo Boudet
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#
# Contributor(s): Jean Coquet, Pierre Vignet, Mateo Boudet

"""Entry point and argument parser for biopax2cadbiom"""

# Standard imports
import os
import sys
import argparse
import pytest
import pkg_resources

# Custom imports
import biopax2cadbiom.biopax_converter as b2c
from biopax2cadbiom import cadbiom_writer
import biopax2cadbiom.commons as cm
import biopax2cadbiom.tools as tools

LOGGER = cm.logger()


def run_tests(dummy_args):
    """Run unit tests. Translate BioPAX test cases to Cadbiom models and
    compare them to the reference models.
    """

    # Run pytest on the current package directory (where 'test' dir is)
    package_dir = pkg_resources.resource_filename(__name__, "../")

    pytest.main([package_dir])
    exit()


def make_model(args):
    """Build a Cadbiom model based on the BioPAX data obtained from a triplestore."""
    # Display graph infos
    if args["get_graph_uris"]:
        tools.get_info_from_triplestore(args)
        return

    # Make model
    b2c.main(args)


def remove_scc(args):
    """Remove Strongly Connected Components from a Cadbiom model."""
    cadbiom_writer.remove_scc_from_model(args["cadbiom_model"])


def args_to_param(args):
    """Return argparse namespace as a dict {variable name: value}"""
    return {k: v for k, v in vars(args).items() if k not in ("func", "verbose")}


def ensure_dir_presence(path):
    """Test write mode and eventually create the given directory"""


    dir_path = os.path.dirname(path)

    if not dir_path:
        # Current directory
        dir_path = "."

    if not os.path.isdir(dir_path):
        LOGGER.info("Creation of the directory <{}>".format(dir_path))
        os.mkdir(dir_path)

    if not os.access(dir_path, os.W_OK):
        LOGGER.error("<{}> is not writable".format(dir_path))
        exit()


def main():
    """Entry point"""

    parser = argparse.ArgumentParser(
        description="biopax2cabiom is a script to transform BioPAX RDF data "
        "from a triplestore to a CADBIOM model."
    )
    # Default log level: info
    parser.add_argument("-vv", "--verbose", nargs="?", default="info")
    # Subparsers
    subparsers = parser.add_subparsers(title="subcommands")

    # subparser: Tests
    # Just write the parameter 'tests'
    parser_run_tests = subparsers.add_parser("tests", help=run_tests.__doc__)
    parser_run_tests.set_defaults(func=run_tests)

    # subparser: Make model
    parser_make_model = subparsers.add_parser(
        "model",
        help=make_model.__doc__,
        formatter_class=argparse.ArgumentDefaultsHelpFormatter,
    )
    # Create argument groups
    triplestore_group = parser_make_model.add_argument_group(title="Triplestore options")
    pickle_group = parser_make_model.add_argument_group(title="Backup options")
    model_group = parser_make_model.add_argument_group(title="Cadbiom model options")

    # Triplestore settings
    group = triplestore_group.add_mutually_exclusive_group(required=True)
    group.add_argument(
        "--get_graph_uris",
        action="store_true",
        help="Get the list of RDF and BioPAX graphs stored on the triplestore.",
    )
    group.add_argument(
        "--graph_uris",
        nargs="+",
        help="List of RDF graphs to be queried on the triplestore.",
    )
    triplestore_group.add_argument(
        "--provenance_uri",
        type=str,
        nargs="?",
        default=None,
        help="URI of the queried subgraphs.",
    )
    triplestore_group.add_argument(
        "--triplestore",
        type=str,
        nargs="?",
        default=cm.SPARQL_PATH,
        help="Address (URL) of the triplestore endpoint.",
    )
    triplestore_group.add_argument(
        "--limit",
        type=int,
        nargs="?",
        default=cm.SPARQL_LIMIT,
        help="Limit the size of data chuncks retrieved by a SPARQL query. "
        "This makes it possible to fine-tune the requests according to the "
        "configuration of the server queried.",
    )

    # Backup of queries
    pickle_group.add_argument(
        "--backup_queries",
        action="store_true",
        help="Allow to save and reload the results of SPARQL queries without "
        "re-interrogating the server. "
        "The goal is to save time and bandwidth on requests as long as "
        "the server data does not change.",
    )
    pickle_group.add_argument(
        "--backup_file",
        type=str,
        nargs="?",
        default="backup.p",
        help="Backup file to save and reload the results of SPARQL queries.",
    )

    # Model options
    model_group.add_argument(
        "--cadbiom_model",
        type=str,
        nargs="?",
        default=cm.DIR_OUTPUT + "model.bcx",
        help="Filepath of the created Cadbiom model."
        "Two models are created by default: A default model, and a model "
        "without Strongly Connected Components. See --no_scc_fix.",
    )
    model_group.add_argument(
        "--full_graph",
        action="store_true",
        help="Convert all entities to Cadbiom nodes, even the members of "
        "classes that aren' involved in reactions.",
    )
    model_group.add_argument(
        "--numeric_compartments",
        action="store_true",
        help="If set, compartment names will be based on numeric values instead "
        "of their real names.",
    )
    model_group.add_argument(
        "--no_scc_fix",
        action="store_true",
        help="If set, strongly connected components will NOT be fixed in a"
        "second model.",
    )
    model_group.add_argument(
        "--blacklist_file",
        type=str,
        nargs="?",
        help="File which provides uris of entities that will be removed from the "
        "model (use it to eliminate cofactors or entities from energy metabolism "
        "for example).",
    )
    model_group.add_argument(
        "--show_metrics",
        action="store_true",
        help="Provide a summary about the data ready to be used to design a model."
    )
    parser_make_model.set_defaults(func=make_model)

    # subparser: Remove SCC
    parser_remove_scc = subparsers.add_parser(
        "remove_scc",
        help=remove_scc.__doc__,
        formatter_class=argparse.ArgumentDefaultsHelpFormatter,
    )
    parser_remove_scc.add_argument(
        "cadbiom_model",
        help="Cadbiom model (.bcx file) from which strongly connected components"
        "will be deleted to give a new model.",
    )
    parser_remove_scc.set_defaults(func=remove_scc)

    # Workaround for sphinx-argparse module that require the object parser
    # before the call of parse_args()
    if "html" in sys.argv:
        return parser

    # Get program args and launch associated command
    args = parser.parse_args()

    # Set log level
    cm.log_level(vars(args)["verbose"])

    # Ultimate check of the presence of the directories & launch associated command
    # Take argparse arguments and put them in a standard dict
    params = args_to_param(args)
    know_directory_variables = ("backup_file", "cadbiom_model")
    [
        ensure_dir_presence(params[var])
        for var in know_directory_variables
        if params.get(var, None)
    ]
    args.func(params)


if __name__ == "__main__":

    main()
