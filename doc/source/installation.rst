************
Installation
************

This program is tested on python 2.7.15. Python3 cannot be used because CadBiom relies on the cryptominisat librairy which requires Python2.


Using Docker
=========

For simplicity, a `Docker image <https://docs.docker.com/get-docker/>`_ is available

* `biopax2cadbiom <https://hub.docker.com/repository/docker/dyliss/biopax2cadbiom>`_

You can access biopax2cadbiom by running the following command:

.. code-block:: bash

   $ docker run -it dyliss/biopax2cadbiom bash

From sources for developers
===========================

.. code-block:: bash

        pip install pytest
        git clone https://gitlab.inria.fr/DYLISS/biopax2cadbiom.git
        python2.7 setup.py develop
        pip install biopax2cadbiom
