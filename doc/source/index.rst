.. BioPAX2Cadbiom documentation master file, created by
   sphinx-quickstart on Sat Feb 10 02:00:51 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

.. https://sphinx-argparse.readthedocs.io/en/stable/

Welcome to BioPAX2Cadbiom's documentation!
==========================================

Biopax2cabiom is a script to transform BioPAX RDF data from a triplestore to a CADBIOM model.

.. toctree::
   :maxdepth: 2
   :caption: Contents:
   
   installation
   usage
   biopax_converter
   tests
   tutorial
   examples
   troubleshooting
   contribute


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
