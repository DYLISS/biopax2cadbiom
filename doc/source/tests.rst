*************************************
Documentation of tests for developers
*************************************

Cases
=====
.. automodule:: tests.test_cases
   :members:

Queries
=======
.. automodule:: tests.test_queries
   :members:

Functions
=========
.. automodule:: tests.test_functions
   :members: