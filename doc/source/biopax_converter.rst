*******************************************
Documentation of the package for developers
*******************************************

biopax_converter
================
.. automodule:: biopax2cadbiom.biopax_converter
   :members:

cadbiom_writer
==============
.. automodule:: biopax2cadbiom.cadbiom_writer
   :members:
   
classes
=======
.. automodule:: biopax2cadbiom.classes
   :members:
   :show-inheritance:

sparql_biopaxQueries
====================
.. automodule:: biopax2cadbiom.sparql_biopaxQueries
   :members:

namespaces
==========
.. automodule:: biopax2cadbiom.namespaces
   :members:

sparql_wrapper
==============
.. automodule:: biopax2cadbiom.sparql_wrapper
   :members:

