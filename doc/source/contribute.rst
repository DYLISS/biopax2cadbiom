**********
Contribute
**********

Any help is welcome!

Most wanted:

 * Additional features
 * Bug fixes
 * Examples

Contributions are gratefully accepted through pull-request.
Please report bugs as issues on the `GitLab <https://gitlab.inria.fr/DYLISS/biopax2cadbiom>`_.

Don’t forget to run tests before committing:

.. code-block:: text

   pytest
