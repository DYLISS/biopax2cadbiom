# Biopax2cadbiom

Convert Biopax data (http://biopax.org) to Cabiom model (http://cadbiom.genouest.org).

Supplementary data: https://data-access.cesgo.org/index.php/s/bIbYc7B1dmnFGCd


## Documentation

Available at [http://cadbiom.genouest.org/biopax2cadbiom](http://cadbiom.genouest.org/biopax2cadbiom).


## Installation

From sources, for development purposes:

	# Install the package for developers...
	$ make dev_install
	# or:
	$ pip install -e .[dev]

Uninstall:
    $ pip uninstall -y biopax2cadbiom

## Docker

For simplicity, a [Docker image](https://docs.docker.com/get-docker/) is available

* [biopax2cadbiom](https://hub.docker.com/repository/docker/dyliss/biopax2cadbiom)

* Another way to obtain biopax2cadbiom
```bash
    $ docker pull registry.gitlab.inria.fr/dyliss/biopax2cadbiom:latest 
```

You can access biopax2cadbiom by running the following command:

```bash
   $ docker run -it dyliss/biopax2cadbiom bash
```
or

```bash
   $ docker run -it registry.gitlab.inria.fr/dyliss/biopax2cadbiom bash
```



## Help

    $ biopax2cadbiom -h
    usage: biopax2cadbiom [-h] [-vv [VERBOSE]] {tests,model,remove_scc} ...

    biopax2cabiom is a script to transform BioPAX RDF data from a triplestore to a
    CADBIOM model.

    optional arguments:
    -h, --help            show this help message and exit
    -vv [VERBOSE], --verbose [VERBOSE]

    subcommands:
    {tests,model,remove_scc}
        tests               Run unit tests. Translate BioPAX test cases to Cadbiom
                            models and compare them to the reference models.
        model               Build a Cadbiom model based on the BioPAX data
                            obtained from a triplestore.
        remove_scc          Remove Strongly Connected Components from a Cadbiom
                            model.


    $ biopax2cadbiom model -h
    usage: biopax2cadbiom model [-h]
                            (--get_graph_uris | --graph_uris GRAPH_URIS [GRAPH_URIS ...])
                            [--provenance_uri [PROVENANCE_URI]]
                            [--triplestore [TRIPLESTORE]] [--limit [LIMIT]]
                            [--backup_queries] [--backup_file [BACKUP_FILE]]
                            [--cadbiom_model [CADBIOM_MODEL]] [--full_graph]
                            [--numeric_compartments] [--no_scc_fix]
                            [--blacklist_file [BLACKLIST_FILE]]

    optional arguments:
    -h, --help            show this help message and exit

    Triplestore options:
    --get_graph_uris      Get the list of RDF and BioPAX graphs stored on the
                            triplestore. (default: False)
    --graph_uris GRAPH_URIS [GRAPH_URIS ...]
                            List of RDF graphs to be queried on the triplestore.
                            (default: None)
    --provenance_uri [PROVENANCE_URI]
                            URI of the queried subgraphs. (default: None)
    --triplestore [TRIPLESTORE]
                            Address (URL) of the triplestore endpoint. (default:
                            http://127.0.0.1:8890/sparql/)
    --limit [LIMIT]       Limit the size of data chuncks retrieved by a SPARQL
                            query. This makes it possible to fine-tune the
                            requests according to the configuration of the server
                            queried. (default: 4000)

    Backup options:
    --backup_queries      Allow to save and reload the results of SPARQL queries
                            without re-interrogating the server. The goal is to
                            save time and bandwidth on requests as long as the
                            server data does not change. (default: False)
    --backup_file [BACKUP_FILE]
                            Backup file to save and reload the results of SPARQL
                            queries. (default: backup.p)

    Cadbiom model options:
    --cadbiom_model [CADBIOM_MODEL]
                            Filepath of the created Cadbiom model.Two models are
                            created by default: A default model, and a model
                            without Strongly Connected Components. See
                            --no_scc_fix. (default: output/model.bcx)
    --full_graph          Convert all entities to Cadbiom nodes, even the
                            members of classes that aren' involved in reactions.
                            (default: False)
    --numeric_compartments
                            If set, compartment names will be based on numeric
                            values instead of their real names. (default: False)
    --no_scc_fix          If set, strongly connected components will NOT be
                            fixed in asecond model. (default: False)
    --blacklist_file [BLACKLIST_FILE]
                            File which provides entities that will be removed from
                            the model (use it to eliminate cofactors or entities
                            from energy metabolism for example). (default: None)


## Examples of command line

Please take a look at the [doc](http://cadbiom.genouest.org/doc/biopax2cadbiom/examples.html).
